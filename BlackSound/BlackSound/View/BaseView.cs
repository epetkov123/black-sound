﻿using BlackSound.Entity;
using BlackSound.Repository;
using BlackSound.Service;
using BlackSound.Tools;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace BlackSound.View
{
    public abstract class BaseView<T> where T : BaseEntity, new()
    {
        public abstract BaseRepository<T> CreateRepo();

        public void Show()
        {
            while (true)
            {
                MenuOptions choice = RenderMenu();
                try
                {
                    switch (choice)
                    {
                        case MenuOptions.Read:
                            {
                                GetAll(GetAllPredicate());
                                break;
                            }
                        case MenuOptions.Create:
                            {
                                Add();
                                break;
                            }
                        case MenuOptions.Update:
                            {
                                Update();
                                break;
                            }
                        case MenuOptions.Delete:
                            {
                                Delete();
                                break;
                            }
                        case MenuOptions.ExtraMenuOption1:
                            {
                                GetAll(ExtraMenuOptionFilter1());
                                break;
                            }
                        case MenuOptions.ExtraMenuOption2:
                            {
                                GetAll(ExtraMenuOptionFilter2());
                                break;
                            }
                        case MenuOptions.ExtraMenuOption3:
                            {
                                GetSharedPlaylists();
                                break;
                            }
                        case MenuOptions.ExtraMenuOption4:
                            {
                                ReadSharedPlaylists();
                                break;
                            }
                        case MenuOptions.ExtraMenuOption5:
                            {
                                StopSharingPlaylist();
                                break;
                            }
                        case MenuOptions.Exit:
                            {
                                return;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.ReadKey(true);
                }
            }
        }

        private MenuOptions RenderMenu()
        {
            while (true)
            {
                Console.Clear();

                if (AuthenticationService.LoggedUser.IsAdmin == false)
                {
                    Console.WriteLine("0. Logout");
                }
                else
                {
                    Console.WriteLine("0. Back");
                }

                Console.WriteLine($"1. All {typeof(T).Name}s");
                Console.WriteLine($"2. Create a {typeof(T).Name}");
                Console.WriteLine($"3. Update a {typeof(T).Name}");
                Console.WriteLine($"4. Delete a {typeof(T).Name}");
                RenderExtraMenuItems();

                string choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        {
                            return MenuOptions.Read;
                        }
                    case "2":
                        {
                            return MenuOptions.Create;
                        }
                    case "3":
                        {
                            return MenuOptions.Update;
                        }
                    case "4":
                        {
                            return MenuOptions.Delete;
                        }
                    case "0":
                        {
                            return MenuOptions.Exit;
                        }
                    default:
                        {
                            if (ShowExtraMenuItems(choice) != MenuOptions.Null)
                            {
                                return ShowExtraMenuItems(choice);
                            }
                            else
                            {
                                Console.WriteLine("Invalid choice.");
                                Console.ReadKey(true);
                                break;
                            }
                        }
                }
            }
        }

        public abstract void RenderExtraMenuItems();

        public abstract MenuOptions ShowExtraMenuItems(string choice);

        private void GetAll(Predicate<T> predicate)
        {
            Console.Clear();
            List<T> items = CreateRepo().GetAll(predicate);

            foreach (T item in items)
            {
                RenderToConsole(item);
                Console.WriteLine("##############################");
            }

            Console.ReadKey(true);
        }

        private void Add()
        {
            Console.Clear();
            T item = ReadFromConsole();

            CreateRepo().Save(item);
            AddConnection(item);

            Console.WriteLine($"{item.GetType().Name} saved successfully.");
            Console.ReadKey(true);
        }

        private void Update()
        {
            Console.Clear();
            GetAll(GetAllPredicate());

            Console.Write("ID: ");
            int id = Convert.ToInt32(Validation.ValidateInput(typeof(int)));

            T item = CreateRepo().GetById(id);
            if (item == null || PreValidate(item))
            {
                Console.Clear();
                Console.WriteLine("Not found or can't be edited.");
                Console.ReadKey(true);
                return;
            }

            T current = ReadFromConsole();
            current.Id = item.Id;
            CreateRepo().Save(current);
            Validate(current);

            Console.WriteLine($"{current.GetType().Name} saved successfully.");
            Console.ReadKey(true);
        }

        private void Delete()
        {
            Console.Clear();
            GetAll(GetAllPredicate());

            Console.Write("Id: ");
            int id = Convert.ToInt32(Validation.ValidateInput(typeof(int)));

            T item = CreateRepo().GetById(id);
            if (item == null || PreValidate(item))
            {
                Console.Clear();
                Console.WriteLine("Not found or can't be deleted.");
                Console.ReadKey();
                return;
            }
            else
            {
                CreateRepo().Delete(item);
                DeleteConnection(item);
                Console.WriteLine($"{item.GetType().Name} deleted successfully.");
            }

            Console.ReadKey(true);
        }

        public virtual void RenderToConsole(T item)
        {
            foreach (PropertyInfo pi in item.GetType().GetProperties())
            {
                Console.WriteLine($"{pi.Name}: {pi.GetValue(item)} ");
            }
        }

        public abstract T ReadFromConsole();

        public abstract void DeleteConnection(T item);

        public abstract void AddConnection(T item);

        public abstract Predicate<T> ExtraMenuOptionFilter1();

        public abstract Predicate<T> ExtraMenuOptionFilter2();

        public virtual Predicate<T> GetAllPredicate()
        {
            return null;
        }

        public abstract void Validate(T item);

        public abstract bool PreValidate(T item);

        public virtual void GetSharedPlaylists()
        {

        }

        public virtual void ReadSharedPlaylists()
        {

        }

        public virtual void StopSharingPlaylist()
        {

        }
    }
}