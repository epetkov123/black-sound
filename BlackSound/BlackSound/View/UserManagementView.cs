﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Entity;
using BlackSound.Repository;
using BlackSound.Service;
using BlackSound.Tools;

namespace BlackSound.View
{
    public class UserManagementView : BaseView<User>
    {
        public override BaseRepository<User> CreateRepo()
        {
            UsersRepository usersRepository = new UsersRepository("users.txt");
            return usersRepository;
        }

        public override void RenderToConsole(User user)
        {
            Console.WriteLine("ID: " + user.Id);
            Console.WriteLine("Username: " + user.Username + " ");
            Console.WriteLine("Email: " + user.Email + " ");
            Console.WriteLine("Is Admin: " + user.IsAdmin + " ");
        }

        public override User ReadFromConsole()
        {
            User user = new User();
            Console.Write("Username: ");
            user.Username = Validation.ValidateInput(typeof(string));

            Console.Write("Password: ");
            user.Password = Validation.ValidateInput(typeof(string));

            Console.Write("Email: ");
            user.Email = Validation.ValidateInput(typeof(string));

            Console.Write("Is Admin (True/False): ");
            user.IsAdmin = Convert.ToBoolean(Validation.ValidateInput(typeof(bool)));

            return user;
        }

        public override void DeleteConnection(User user)
        {
            UsersRepository usersRepository = new UsersRepository("users.txt");
            usersRepository.DeleteUsersPlaylist(user);
        }

        public override void AddConnection(User item)
        {
        }

        public override void RenderExtraMenuItems()
        {

        }

        public override MenuOptions ShowExtraMenuItems(string choice)
        {
            return MenuOptions.Null;
        }

        public override Predicate<User> ExtraMenuOptionFilter1()
        {
            return null;
        }

        public override Predicate<User> ExtraMenuOptionFilter2()
        {
            return null;
        }

        public override void Validate(User item)
        {

        }

        public override bool PreValidate(User user)
        {
            if (user.Id == 1 || user.Id == AuthenticationService.LoggedUser.Id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
