﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.View
{
    public class AdminView
    {
        public void Show()
        {
            while (true)
            {
                while (true)
                {
                    Console.Clear();

                    while (true)
                    {
                        Console.Clear();
                        Console.WriteLine("1. Users");
                        Console.WriteLine("2. Songs");
                        Console.WriteLine("3. Playlists");
                        Console.WriteLine("4. Logout");

                        string choice = Console.ReadLine();
                        switch (choice.ToUpper())
                        {
                            case "1":
                                {
                                    UserManagementView view = new UserManagementView();
                                    view.Show();

                                    break;
                                }
                            case "2":
                                {
                                    SongManagementView view = new SongManagementView();
                                    view.Show();

                                    break;
                                }
                            case "3":
                                {
                                    PlaylistManagementView view = new PlaylistManagementView();
                                    view.Show();

                                    break;
                                }
                            case "4":
                                {
                                    return;
                                }
                            default:
                                {
                                    Console.WriteLine("Invalid choice.");
                                    Console.ReadKey(true);
                                    break;
                                }
                        }
                    }
                }
            }
        }
    }
}
