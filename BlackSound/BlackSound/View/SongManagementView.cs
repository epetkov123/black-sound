﻿using BlackSound.Entity;
using BlackSound.Repository;
using BlackSound.Service;
using BlackSound.Tools;
using System;

namespace BlackSound.View
{
    public class SongManagementView : BaseView<Song>
    {
        public override BaseRepository<Song> CreateRepo()
        {
            SongsRepository songsRepository = new SongsRepository("songs.txt");
            return songsRepository;
        }

        public override Song ReadFromConsole()
        {
            Song song = new Song();
            Console.Write("Title: ");
            song.Title = Validation.ValidateInput(typeof(string));

            Console.Write("First name: ");
            song.FirstName = Validation.ValidateInput(typeof(string));

            Console.Write("Last name: ");
            song.LastName = Validation.ValidateInput(typeof(string));

            Console.Write("Year: ");
            song.Year = Convert.ToInt32(Validation.ValidateInput(typeof(bool)));

            return song;
        }

        public override void AddConnection(Song song)
        {
            SongsRepository songsRepository = new SongsRepository("songs.txt");
            songsRepository.AddPlaylistSong(song);
        }

        public override void DeleteConnection(Song song)
        {
            SongsRepository songsRepository = new SongsRepository("songs.txt");
            songsRepository.DeletePlaylistSong(song);
        }

        public override void RenderExtraMenuItems()
        {

        }

        public override MenuOptions ShowExtraMenuItems(string choice)
        {
            return MenuOptions.Null;
        }

        public override Predicate<Song> ExtraMenuOptionFilter1()
        {
            return null;
        }

        public override Predicate<Song> ExtraMenuOptionFilter2()
        {
            return null;
        }

        public override void Validate(Song item)
        {

        }

        public override bool PreValidate(Song item)
        {
            return false;
        }
    }
}
