﻿using BlackSound.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using BlackSound.Repository;
using BlackSound.Entity;
using BlackSound.Service;
using System.Reflection;

namespace BlackSound.View
{
    public class PlaylistManagementView : BaseView<Playlist>
    {
        public override BaseRepository<Playlist> CreateRepo()
        {
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            return playlistsRepository;
        }

        public override void RenderExtraMenuItems()
        {
            Console.WriteLine("5. Public Playlists");
            Console.WriteLine("6. \"All songs\" Playlist");
            Console.WriteLine("7. Shared Playlists");
            Console.WriteLine("8. Share a playlist");
            Console.WriteLine("9. Stop sharing a playlist");
        }

        public override MenuOptions ShowExtraMenuItems(String choice)
        {
            switch (choice)
            {
                case "5":
                    {
                        return MenuOptions.ExtraMenuOption1;
                    }
                case "6":
                    {
                        return MenuOptions.ExtraMenuOption2;
                    }
                case "7":
                    {
                        return MenuOptions.ExtraMenuOption3;
                    }
                case "8":
                    {
                        return MenuOptions.ExtraMenuOption4;
                    }
                case "9":
                    {
                        return MenuOptions.ExtraMenuOption5;
                    }
                default:
                    {
                        return MenuOptions.Null;
                    }
            }
        }

        public override Playlist ReadFromConsole()
        {
            Playlist playlist = new Playlist();
            Console.Write("Name: ");
            playlist.Name = Validation.ValidateInput(typeof(string));

            Console.Write("Description: ");
            playlist.Description = Validation.ValidateInput(typeof(string));

            Console.Write("Is public?: ");
            playlist.IsPublic = Convert.ToBoolean(Validation.ValidateInput(typeof(bool)));

            playlist.OwnerId = AuthenticationService.LoggedUser.Id;
            return playlist;
        }

        public override void RenderToConsole(Playlist playlist)
        {
            Console.WriteLine("ID: " + playlist.Id);
            Console.WriteLine("Name: " + playlist.Name);
            Console.WriteLine("Description: " + playlist.Description);
            RenderSongs(playlist.Id);
        }

        private void RenderSongs(int playlistId)
        {
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            List<Song> songs = playlistsRepository.GetSongs(playlistId);

            Console.WriteLine("------------------------------");
            Console.WriteLine("Songs:");

            foreach (Song song in songs)
            {
                Console.WriteLine(song.Title + " " + song.FirstName + " " + song.LastName + " " + song.Year);
            }
        }

        public override void DeleteConnection(Playlist playlist)
        {
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            playlistsRepository.DeletePlaylistSong(playlist);
        }

        public override void AddConnection(Playlist playlist)
        {
            SongsRepository songsRepository = new SongsRepository("songs.txt");
            List<Song> songs = songsRepository.GetAll();
            List<int> songIds = ReadSongs(songs);
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            playlistsRepository.AddPlaylistSong(playlist, songIds);
        }

        private List<int> ReadSongs(List<Song> songs)
        {
            Console.WriteLine("---------");
            Console.WriteLine("Songs:");

            foreach (Song song in songs)
            {
                Console.WriteLine(song.Id + " " + song.Title + " " + song.FirstName + " " + song.LastName + " " + song.Year);
            }

            Console.Write("Pick song IDs(ex. \"1 2 3\"): ");
            List<int> songIds = new List<int>();

            do
            {
                try
                {
                    songIds = Validation.CheckNullInput()
                    .Split()
                    .Select(int.Parse)
                    .ToList();
                }
                catch (Exception)
                {
                    Console.Write("Please enter valid integer numbers: ");
                }
            }
            while (!songIds.Any() && !Validation.CheckIfContainsSongs());

            return songIds;
        }

        public override Predicate<Playlist> ExtraMenuOptionFilter1()
        {
            return (i => i.IsPublic == true);
        }

        public override Predicate<Playlist> ExtraMenuOptionFilter2()
        {
            return (i => i.Id == 1);
        }

        public override Predicate<Playlist> GetAllPredicate()
        {
            return (i => AuthenticationService.LoggedUser.Id == i.OwnerId);
        }

        public override void Validate(Playlist playlist)
        {
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            Console.Write("Do you want to edit songs? (Y/N): ");

            string choice = Console.ReadLine();
            List<int> songIds = new List<int>();

            switch (choice.ToUpper())
            {
                case "Y":
                    {
                        Console.Write("Do you want to add or remove songs? (A/R): ");
                        string addOrEdit = Console.ReadLine();

                        switch (addOrEdit.ToUpper())
                        {
                            case "A":
                                {
                                    SongsRepository songsRepository = new SongsRepository("songs.txt");
                                    List<Song> songs = songsRepository.GetAll();
                                    songIds = ReadSongs(songs);
                                    playlistsRepository.AddPlaylistSong(playlist, songIds);
                                    return;
                                }
                            case "R":
                                {
                                    List<Song> songs = playlistsRepository.GetSongs(playlist.Id);
                                    songIds = ReadSongs(songs);
                                    playlistsRepository.Remove(playlist.Id, songIds);
                                    return;
                                }
                            default:
                                {
                                    Console.WriteLine("Invalid choice.");
                                    Console.ReadKey(true);
                                    break;
                                }
                        }

                        break;
                    }
                case "N":
                    {
                        Console.ReadKey(true);
                        return;
                    }
                default:
                    {
                        Console.WriteLine("Invalid choice.");
                        Console.ReadKey(true);
                        break;
                    }
            }

        }

        public override bool PreValidate(Playlist playlist)
        {
            if (playlist.OwnerId != AuthenticationService.LoggedUser.Id || playlist.Id == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void GetSharedPlaylists()
        {
            Console.Clear();
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            List<Playlist> playlists = playlistsRepository.GetSharedById(AuthenticationService.LoggedUser.Id);

            foreach (Playlist playlist in playlists)
            {
                RenderToConsole(playlist);
                //Console.WriteLine();
            }
            Console.ReadKey(true);
        }

        public override void ReadSharedPlaylists()
        {
            Console.Clear();
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            List<Playlist> playlists = playlistsRepository.GetAll(i => i.OwnerId == AuthenticationService.LoggedUser.Id);

            foreach (Playlist playlist in playlists)
            {
                RenderToConsole(playlist);
                Console.WriteLine();
            }

            Console.Write("Pick a playlist ID: ");
            int playlistId = 0;

            try
            {
                playlistId = Convert.ToInt32(Validation.ValidateInput(typeof(int)));
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid integer number");
            }

            UsersRepository usersRepository = new UsersRepository("users.txt");
            List<User> users = usersRepository.GetAll();

            Console.WriteLine();
            foreach (User user in users)
            {
                foreach (PropertyInfo pi in user.GetType().GetProperties())
                {
                    Console.WriteLine($"{pi.Name}: {pi.GetValue(user)} ");
                }
                Console.WriteLine();
            }

            Console.Write("Pick a user ID: ");
            int sharedUserId = 0;

            try
            {
                sharedUserId = Convert.ToInt32(Validation.ValidateInput(typeof(int)));
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid integer number");
            }

            if (sharedUserId != 0 && playlistId != 0)
            {
                playlistsRepository.AddSharedPlaylists(AuthenticationService.LoggedUser.Id, sharedUserId, playlistId);
                Console.WriteLine("Sharing saved successfully.");
            }
            Console.ReadKey(true);
        }

        public override void StopSharingPlaylist()
        {
            Console.Clear();
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            List <PlaylistUser> playlistUsers= playlistsRepository.GetSharingById(AuthenticationService.LoggedUser.Id);

            foreach(PlaylistUser playlistUser in playlistUsers)
            {
                Console.WriteLine("Id: " + playlistUser.Id);
                Console.WriteLine("User shared with: " + playlistUser.Id);
                Console.WriteLine("Playlist: ");

                Playlist playlist = playlistsRepository.GetById(playlistUser.Id);
                RenderToConsole(playlist);
            }

            Console.Write("Pick a share ID: ");
            int shareId = 0;

            try
            {
                shareId = Convert.ToInt32(Validation.ValidateInput(typeof(int)));
            }
            catch (Exception)
            {
                Console.WriteLine("Please enter a valid integer number");
            }

            if (shareId != 0)
            {
                //playlistsRepository.DeleteSharedPlaylists(AuthenticationService.LoggedUser.Id, sharedUserId, playlistId);
                Console.WriteLine("Stop sharing saved successfully.");
            }
            Console.ReadKey(true);
        }
    }
}