﻿using BlackSound.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Service
{
    public class Validation
    {
        public static string ValidateInput(Type type)
        {
            string input = CheckNullInput();
            while (!IsType(input, type))
            {
                Console.Write("Please enter a valid value: ");
                input = CheckNullInput();
            }
            return input;
        }

        public static bool IsType(string input, Type type)
        {
            try
            {
                TypeDescriptor.GetConverter(type).ConvertFromString(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string CheckNullInput()
        {
            string input = "";
            while (!input.Any())
            {
                input = Console.ReadLine();
                if (!input.Any())
                {
                    Console.Write("You need to enter input: ");
                }
            }
            return input;
        }

        public static bool CheckIfContainsSongs()
        {
            return true;
        }

        public static string PasswordMasking()
        {
            string password = "";
            ConsoleKeyInfo key;
            do
            {
                key = Console.ReadKey(true);

                if (!char.IsControl(key.KeyChar))
                {
                    password += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && password.Length > 0)
                    {
                        password = password.Substring(0, (password.Length - 1));
                        Console.Write("\b \b");
                    }
                }
            }
            while (key.Key != ConsoleKey.Enter);
            Console.WriteLine();

            return password;
        }
    }
}
