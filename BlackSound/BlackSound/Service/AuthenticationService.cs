﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackSound.Entity;
using BlackSound.Repository;

namespace BlackSound.Service
{
    public static class AuthenticationService
    {
        public static User LoggedUser { get; set; }

        public static void AuthenticateUser(string username, string password)
        {
            UsersRepository userRepo = new UsersRepository("users.txt");
            LoggedUser = userRepo.GetByUsernameAndPassword(username, password);
        }
    }
}
