﻿using BlackSound.Entity;

namespace BlackSound.Repository
{
    public class SongsRepository : BaseRepository<Song>
    {
        public SongsRepository(string filePath)
            : base(filePath)
        {

        }

        public void AddPlaylistSong(Song item)
        {
            PlaylistSongRepository playlistSongRepository = new PlaylistSongRepository("playlistsong.txt");
            playlistSongRepository.Insert(1, item.Id);
        }

        public void DeletePlaylistSong(Song item)
        {
            PlaylistSongRepository playlistSongRepository = new PlaylistSongRepository("playlistsong.txt");
            playlistSongRepository.Delete(item);
        }
    }
}
