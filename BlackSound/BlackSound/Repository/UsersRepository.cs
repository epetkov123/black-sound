﻿using System;
using System.Collections.Generic;
using System.IO;
using BlackSound.Entity;

namespace BlackSound.Repository
{
    public class UsersRepository : BaseRepository<User>
    {
        public UsersRepository(string filePath)
            : base(filePath)
        {

        }

        public void DeleteUsersPlaylist(User item)
        {
            PlaylistsRepository playlistsRepository = new PlaylistsRepository("playlists.txt");
            List<Playlist> playlists = playlistsRepository.GetAll(i => i.OwnerId == item.Id);

            foreach (Playlist playlist in playlists)
            {
                playlistsRepository.Delete(playlist);
            }
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    User user = new User();
                    user.Email = sr.ReadLine();
                    user.Username = sr.ReadLine();
                    user.Password = sr.ReadLine();
                    user.IsAdmin = Convert.ToBoolean(sr.ReadLine());
                    user.Id = Convert.ToInt32(sr.ReadLine());

                    if (user.Username == username && user.Password == password)
                    {
                        return user;
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }
    }
}
