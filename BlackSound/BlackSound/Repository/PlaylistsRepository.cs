﻿using BlackSound.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repository
{
    public class PlaylistsRepository : BaseRepository<Playlist>
    {
        public PlaylistsRepository(string filePath)
            : base(filePath)
        {

        }

        public void DeletePlaylistSong(Playlist item)
        {
            PlaylistSongRepository playlistSong = new PlaylistSongRepository("playlistsong.txt.");
            playlistSong.Delete(item);
        }

        public void AddPlaylistSong(Playlist item, List<int> ids)
        {
            PlaylistSongRepository playlistSongRepository = new PlaylistSongRepository("playlistsong.txt");
            foreach (int id in ids)
            {
                playlistSongRepository.Insert(item.Id, id);
            }
        }

        public void Remove(int playlistId, List<int> ids)
        {
            PlaylistSongRepository playlistSongRepository = new PlaylistSongRepository("playlistsong.txt");
            playlistSongRepository.Delete(playlistId, ids);
        }

        public List<Song> GetSongs(int id)
        {
            PlaylistSongRepository playlistSongRepository = new PlaylistSongRepository("playlistsong.txt");
            List<Song> songs = playlistSongRepository.GetSongs(id);
            return songs;
        }

        public void AddSharedPlaylists(int sharingUserId, int sharedUserId, int playlistId)
        {
            PlaylistUserRepository playlistUserRepository = new PlaylistUserRepository("playlistuser.txt");
            playlistUserRepository.Insert(sharingUserId, sharedUserId, playlistId);
        }

        public List<PlaylistUser> GetSharingById(int userId)
        {
            PlaylistUserRepository playlistUserRepository = new PlaylistUserRepository("playlistuser.txt");
            List<PlaylistUser> sharingPlaylists = playlistUserRepository.GetSharingPlaylists(userId);

            return sharingPlaylists;
        }

        public List<Playlist> GetSharedById(int userId)
        {
            PlaylistUserRepository playlistUserRepository = new PlaylistUserRepository("playlistuser.txt");
            List<int> sharedPlaylistIds = playlistUserRepository.GetSharedPlaylistsIds(userId);

            List<Playlist> playlists = new List<Playlist>();
            foreach(int id in sharedPlaylistIds)
            {
                playlists.Add(GetById(id));
            }

            return playlists;
        }
    }
}