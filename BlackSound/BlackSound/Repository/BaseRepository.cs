﻿using BlackSound.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repository
{
    public abstract class BaseRepository<T> where T : BaseEntity, new()
    {
        protected readonly string filePath;

        public BaseRepository(string filePath)
        {
            this.filePath = filePath;
        }

        protected int GetNextId()
        {
            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            int id = 1;

            try
            {
                while (!sr.EndOfStream)
                {
                    T item = new T();

                    foreach (PropertyInfo pi in item.GetType().GetProperties())
                    {
                        pi.SetValue(item, Convert.ChangeType(sr.ReadLine(), pi.PropertyType));
                    }

                    if (id <= item.Id)
                    {
                        id = item.Id + 1;
                    }
                }
            }

            finally
            {
                sr.Close();
                fs.Close();
            }

            return id;
        }

        private void Update(T item)
        {
            string tempFilePath = "temp." + filePath;

            FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(ifs);

            FileStream ofs = new FileStream(tempFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(ofs);

            try
            {
                while (!sr.EndOfStream)
                {
                    T current = new T();
                    foreach (PropertyInfo pi in current.GetType().GetProperties())
                    {
                        pi.SetValue(current, Convert.ChangeType(sr.ReadLine(), pi.PropertyType));
                    }

                    if (current.Id != item.Id)
                    {
                        foreach (PropertyInfo pi in current.GetType().GetProperties())
                        {
                            sw.WriteLine(pi.GetValue(current));
                        }
                    }
                    else
                    {
                        foreach (PropertyInfo pi in item.GetType().GetProperties())
                        {
                            sw.WriteLine(pi.GetValue(item));
                        }
                    }
                }
            }
            finally
            {
                sw.Close();
                ofs.Close();
                sr.Close();
                ifs.Close();
            }

            File.Delete(filePath);
            File.Move(tempFilePath, filePath);
        }

        private void Insert(T item)
        {
            item.Id = GetNextId();

            FileStream fs = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);

            try
            {
                foreach (PropertyInfo pi in item.GetType().GetProperties())
                {
                    sw.WriteLine(pi.GetValue(item));
                }
            }
            finally
            {
                sw.Close();
                fs.Close();
            }
        }

        public T GetById(int id)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    T item = new T();
                    foreach (PropertyInfo pi in item.GetType().GetProperties())
                    {
                        pi.SetValue(item, Convert.ChangeType(sr.ReadLine(), pi.PropertyType));
                    }
                    if (item.Id == id)
                    {
                        return item;
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }

        public List<T> GetAll(Predicate<T> filter = null)
        {
            List<T> result = new List<T>();

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    T item = new T();
                    foreach (PropertyInfo pi in item.GetType().GetProperties())
                    {
                        pi.SetValue(item, Convert.ChangeType(sr.ReadLine(), pi.PropertyType));
                    }
                    if (filter == null || filter(item))
                        result.Add(item);
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }

        public void Delete(T item)
        {
            string tempFilePath = "temp." + filePath;

            FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(ifs);

            FileStream ofs = new FileStream(tempFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(ofs);

            try
            {
                while (!sr.EndOfStream)
                {
                    T current = new T();
                    foreach (PropertyInfo pi in current.GetType().GetProperties())
                    {
                        pi.SetValue(current, Convert.ChangeType(sr.ReadLine(), pi.PropertyType));
                    }

                    if (current.Id != item.Id)
                    {
                        foreach (PropertyInfo pi in item.GetType().GetProperties())
                        {
                            sw.WriteLine(pi.GetValue(current));
                        }
                    }
                }
            }
            finally
            {
                sw.Close();
                ofs.Close();
                sr.Close();
                ifs.Close();
            }

            File.Delete(filePath);
            File.Move(tempFilePath, filePath);
        }

        public void Save(T item)
        {
            if (item.Id > 0)
            {
                Update(item);
            }
            else
            {
                Insert(item);
            }
        }
    }
}
