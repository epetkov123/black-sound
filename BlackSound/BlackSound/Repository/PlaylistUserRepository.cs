﻿using BlackSound.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repository
{
    public class PlaylistUserRepository
    {
        private readonly string filePath;

        public PlaylistUserRepository(string filePath)
        {
            this.filePath = filePath;
        }

        private int GetNextId()
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            int id = 1;
            try
            {
                while (!sr.EndOfStream)
                {
                    PlaylistUser playlistUser = new PlaylistUser();
                    playlistUser.Id = Convert.ToInt32(sr.ReadLine());
                    playlistUser.SharingUserId = Convert.ToInt32(sr.ReadLine());
                    playlistUser.SharedUserId = Convert.ToInt32(sr.ReadLine());
                    playlistUser.SharedPlaylistId = Convert.ToInt32(sr.ReadLine());

                    if (id <= playlistUser.Id)
                    {
                        id = playlistUser.Id + 1;
                    }
                }
            }

            finally
            {
                sr.Close();
                fs.Close();
            }

            return id;
        }

        public void Insert(int sharingUserId, int sharedUserId, int playlistId)
        {
            int id = GetNextId();

            FileStream fs = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);

            try
            {
                sw.WriteLine(id);
                sw.WriteLine(sharingUserId);
                sw.WriteLine(sharedUserId);
                sw.WriteLine(playlistId);
            }
            finally
            {
                sw.Close();
                fs.Close();
            }
        }

        public List<PlaylistUser> GetSharingPlaylists(int userId)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            List<PlaylistUser> sharedPlaylists = new List<PlaylistUser>();
            try
            {
                while (!sr.EndOfStream)
                {
                    PlaylistUser playlistUser = new PlaylistUser();
                    playlistUser.Id = Convert.ToInt32(sr.ReadLine());
                    playlistUser.SharingUserId = Convert.ToInt32(sr.ReadLine());
                    playlistUser.SharedUserId = Convert.ToInt32(sr.ReadLine());
                    playlistUser.SharedPlaylistId = Convert.ToInt32(sr.ReadLine());

                    if (playlistUser.SharingUserId == userId)
                    {
                        sharedPlaylists.Add(playlistUser);
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return sharedPlaylists;
        }

        public List<int> GetSharedPlaylistsIds(int userId)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            List<int> sharedPlaylistsIds = new List<int>();
            try
            {
                while (!sr.EndOfStream)
                {
                    int playlistUserId = Convert.ToInt32(sr.ReadLine());
                    int sharingUserId = Convert.ToInt32(sr.ReadLine());
                    int sharedUserId = Convert.ToInt32(sr.ReadLine());
                    int sharedPlaylistId = Convert.ToInt32(sr.ReadLine());

                    if (sharedUserId == userId)
                    {
                        sharedPlaylistsIds.Add(sharedPlaylistId);
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return sharedPlaylistsIds;
        }
    }
}
