﻿using BlackSound.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Repository
{
    public class PlaylistSongRepository
    {
        private readonly string filePath;

        public PlaylistSongRepository(string filePath)
        {
            this.filePath = filePath;
        }

        public void Insert(int playlistId, int songId)
        {
            int id = GetNextId();

            FileStream fs = new FileStream(filePath, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);

            try
            {
                sw.WriteLine(id);
                sw.WriteLine(playlistId);
                sw.WriteLine(songId);
            }
            finally
            {
                sw.Close();
                fs.Close();
            }
        }

        public void Delete(int playlistId, List<int> ids)
        {
            string tempFilePath = "temp." + filePath;

            FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(ifs);

            FileStream ofs = new FileStream(tempFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(ofs);

            try
            {
                while (!sr.EndOfStream)
                {
                    foreach (int id in ids)
                    {
                        PlaylistSong playlistSong = new PlaylistSong();
                        playlistSong.Id = Convert.ToInt32(sr.ReadLine());
                        playlistSong.PlaylistId = Convert.ToInt32(sr.ReadLine());
                        playlistSong.SongId = Convert.ToInt32(sr.ReadLine());

                        if (playlistSong.SongId != id || playlistSong.PlaylistId != playlistId)
                        {
                            sw.WriteLine(playlistSong.Id);
                            sw.WriteLine(playlistSong.PlaylistId);
                            sw.WriteLine(playlistSong.SongId);
                        }
                    }
                }
            }
            finally
            {
                sw.Close();
                ofs.Close();
                sr.Close();
                ifs.Close();
            }

            File.Delete(filePath);
            File.Move(tempFilePath, filePath);
        }

        public void Delete(Song item)
        {
            string tempFilePath = "temp." + filePath;

            FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(ifs);

            FileStream ofs = new FileStream(tempFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(ofs);

            try
            {
                while (!sr.EndOfStream)
                {
                    PlaylistSong playlistSong = new PlaylistSong();
                    playlistSong.Id = Convert.ToInt32(sr.ReadLine());
                    playlistSong.PlaylistId = Convert.ToInt32(sr.ReadLine());
                    playlistSong.SongId = Convert.ToInt32(sr.ReadLine());

                    if (playlistSong.SongId != item.Id)
                    {
                        sw.WriteLine(playlistSong.Id);
                        sw.WriteLine(playlistSong.PlaylistId);
                        sw.WriteLine(playlistSong.SongId);
                    }
                }
            }
            finally
            {
                sw.Close();
                ofs.Close();
                sr.Close();
                ifs.Close();
            }

            File.Delete(filePath);
            File.Move(tempFilePath, filePath);
        }

        public void Delete(Playlist item)
        {
            string tempFilePath = "temp." + filePath;

            FileStream ifs = new FileStream(filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(ifs);

            FileStream ofs = new FileStream(tempFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(ofs);

            try
            {
                while (!sr.EndOfStream)
                {
                    PlaylistSong playlistSong = new PlaylistSong();
                    playlistSong.Id = Convert.ToInt32(sr.ReadLine());
                    playlistSong.PlaylistId = Convert.ToInt32(sr.ReadLine());
                    playlistSong.SongId = Convert.ToInt32(sr.ReadLine());

                    if (playlistSong.PlaylistId != item.Id)
                    {
                        sw.WriteLine(playlistSong.Id);
                        sw.WriteLine(playlistSong.PlaylistId);
                        sw.WriteLine(playlistSong.SongId);
                    }
                }
            }
            finally
            {
                sw.Close();
                ofs.Close();
                sr.Close();
                ifs.Close();
            }

            File.Delete(filePath);
            File.Move(tempFilePath, filePath);
        }

        private int GetNextId()
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            int id = 1;
            try
            {
                while (!sr.EndOfStream)
                {
                    PlaylistSong playlistSong = new PlaylistSong();
                    playlistSong.Id = Convert.ToInt32(sr.ReadLine());
                    playlistSong.PlaylistId = Convert.ToInt32(sr.ReadLine());
                    playlistSong.SongId = Convert.ToInt32(sr.ReadLine());

                    if (id <= playlistSong.Id)
                    {
                        id = playlistSong.Id + 1;
                    }
                }
            }

            finally
            {
                sr.Close();
                fs.Close();
            }

            return id;
        }

        public List<Song> GetSongs(int id)
        {
            List<int> songIds = GetSongsId(id);
            List<Song> result = new List<Song>();

            FileStream fs = new FileStream("songs.txt", FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            if (!songIds.Any())
            {
                sr.Close();
                fs.Close();
                return result;
            }

            try
            {
                while (!sr.EndOfStream)
                {

                    Song song = new Song();
                    song.Title = sr.ReadLine();
                    song.FirstName = sr.ReadLine();
                    song.LastName = sr.ReadLine();
                    song.Year = Convert.ToInt32(sr.ReadLine());
                    song.Id = Convert.ToInt32(sr.ReadLine());
                    foreach (int songId in songIds)
                    {
                        if (songId == song.Id)
                        {
                            result.Add(song);
                        }
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }

        public List<int> GetSongsId(int id)
        {
            List<int> songIds = new List<int>();

            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    int playlistSongId = Convert.ToInt32(sr.ReadLine());
                    int playlistId = Convert.ToInt32(sr.ReadLine());
                    int songId = Convert.ToInt32(sr.ReadLine());

                    if (playlistId == id)
                    {
                        songIds.Add(songId);
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return songIds;
        }
    }
}