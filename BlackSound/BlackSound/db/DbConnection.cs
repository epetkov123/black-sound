﻿using System.Data.SqlClient;

namespace BlackSound.db
{
    public class DbConnection
    {
        private static string host = "(localdb)\\ProjectsV13";
        private static string catalog = "BlackSoundDb";

        private static string connectionString = @"Server=(localdb)\\Projects;Database=MinionsDB;Integrated Security=True";

        public static void InitialSetup()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                ExecNonQuery(connection, SqlQueries.databaseSql);

                //connection.ChangeDatabase("BlackSoundDb");

                ExecNonQuery(connection, SqlQueries.tableUsers);

                ExecNonQuery(connection, SqlQueries.insertUsers);

                connection.Close();
            }
        }

        private static void ExecNonQuery(SqlConnection connection, string databaseSql)
        {
            using (SqlCommand command = new SqlCommand(databaseSql, connection))
            {
                command.ExecuteNonQuery();
            }
        }
    }
}

