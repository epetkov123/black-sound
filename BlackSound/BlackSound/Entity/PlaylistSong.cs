﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Entity
{
    public class PlaylistSong : BaseEntity
    {
        public int PlaylistId { get; set; }
        public int SongId { get; set; }
    }
}