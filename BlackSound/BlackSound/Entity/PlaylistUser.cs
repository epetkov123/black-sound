﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Entity
{
    public class PlaylistUser : BaseEntity
    {
        public int SharingUserId { get; set; }
        public int SharedUserId { get; set; }
        public int SharedPlaylistId { get; set; }
    }
}
