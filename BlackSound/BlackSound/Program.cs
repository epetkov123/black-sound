﻿using System;
using BlackSound.View;
using BlackSound.Service;
using System.Data.SqlClient;
using BlackSound.db;
using System.Data;

namespace BlackSound
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                DbConnection.InitialSetup();

                LoginView loginView = new LoginView();
                loginView.Show();

                if (AuthenticationService.LoggedUser.IsAdmin)
                {
                    AdminView adminView = new AdminView();
                    adminView.Show();
                }
                else
                {
                    PlaylistManagementView userView = new PlaylistManagementView();
                    userView.Show();
                }
            }
        }
    }
}
