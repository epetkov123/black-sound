﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackSound.Tools
{
    public enum MenuOptions
    {
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4,
        ExtraMenuOption1 = 5,
        ExtraMenuOption2 = 6,
        ExtraMenuOption3 = 7,
        ExtraMenuOption4 = 8,
        ExtraMenuOption5 = 9,
        Exit = 10,
        Null = 11
    }
}
